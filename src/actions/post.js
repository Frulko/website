import {RECEIVE_ONE_POST} from '../actionTypes'
import fetch from 'isomorphic-fetch'
import moment from 'moment'

function receiveOnePost (post_id, post) {
	return {
		type: RECEIVE_ONE_POST,
		post
	};
}

function shouldFetchPostByID(state, id) {
	return true;
}

export function fetchPostByID(id) {
	console.log('fetchPostByID', id);

	if (typeof id === 'object') {
		id = id.articleID;
	}

	return (dispatch) => {
		return fetch(`http://localhost:3000/api/tweet/${id}`).
		then(function(response){
			return response.json();
		}).
		then((response) => {
			console.log('receiveOnePost', id, response);
			return dispatch(receiveOnePost(id, response));
		});
	};
}

/*export function fetchPosts() {
  return (dispatch) => {
    return fetch(`http://localhost:3000/api/tweets`).
      then((response) => response.json()).
      then(function(response) {
      	response  = response.map(function(item, index) {
      		item.alt = (index % 2 == 0);
      		return item;
      	});

      	return dispatch(receivePosts(response));
      });
  };
}*/

export function fetchPostIfNeeded(id) {
	console.log('fetchPostIfNeeded', id);
	return (dispatch, getState) => {
		if (shouldFetchPostByID(getState(), id)) {
			return dispatch(fetchPostByID(id))
		}
	}
}