export default class Player {
    constructor(options) {
        let {name} = options;
        this.name = name;
        this.cards = [];
        this.id = null;
    }

    addCard(cardInstance) {
        this.cards.push(cardInstance);
    }
}
