import Deck from './Deck';

export default class Game {
    constructor() {
        this.deck = new Deck();
        this.usedCards = [];
        this.players = [];
        this.nbCardsPerPlayer = 8;
        this.nbMaxViewedCards = 4;
        this.currentPlayerId = null;
    }

    addPlayer(playerInstance) {
        /*
            Ajout d'un joueur dans la partie
        */
        this.players.push(playerInstance);
        return this.players.length-1;
    }

    distributeCards () {
        /*
            Distribution des cartes à chaque joueur
        */
        for(var i = 0, l = this.nbCardsPerPlayer; i < l; i++) {
            for(var playerIndex in this.players) {
                var player = this.players[playerIndex];
                var card = this.deck.cards.shift();
                card.inHand = true;
                player.addCard(card);
            }
        }
    }

    takeCard (playerId) {
        /*
            Prendre une carte dans le deck
        */
        var card = this.deck.cards.shift();
        console.log(this.players[playerId].name + ' - take:');
        //this.usedCards.push(card);
        return card;
    }

    throwCard (takeCard) {
        /*
            Jeter une carte dans la défausse
        */
        this.usedCards.push(card);
    }

    throwCardFromUserCards(playerId, userCardId) {
        /*
            Jeter une card de son jeu, si c'est la même que la dernière de défausse OK, sinon on prends une carte supplémentaire
        */
        var player = this.getPlayerFromId(playerId);


        var card = player.cards[userCardId];
        console.log(this.usedCards[this.usedCards.length - 1].value, card.value);
        if (this.usedCards[this.usedCards.length - 1].value === card.value) {
            console.log('card->');
            this.throwCard(card);
        } else {
            console.log('player->');
            player.addCard(this.deck.cards.shift());
        }
    }

    exchangeCard(takeCard, playerId, playerCardId) {
        /*
            Echanger la carte du deck contre une de son jeu
        */
        var playerCard = this.getPlayerFromId(playerId).cards[playerCardId];
        this.players[playerId].cards[playerCardId] = takeCard;
        this.throwCard(playerCard);
    }

    showCards(playerId) {
        /*
            Regarder ses dernière cartes // TODO inverser sens ici c'est les premières
        */
        var player = this.getPlayerFromId(playerId);
        for(var i=0; i<this.nbMaxViewedCards; i++) {
            var card = player.cards[i];
            console.log('Card:', card.valueName, card.skinName);
        }
    }

    getLastCardUsed() {
        /*
            Prendre la dernière carte de la défausse
        */
        return this.usedCards.pop();
    }

    getPlayerFromId(playerId) {
        /*
            Récupérer l'instance de joueur par rapport à son ID
        */
        return this.players[playerId];
    }

    randomPlayerId() {
        let o = (Math.random() * this.players.length) + 1;
        return (Math.floor(o) - 1);
    }

    start() {
        //TODO checker plus d'un joueur pour pouvoir commencer
        this.distributeCards();
        //console.log();
        this.currentPlayerId = this.randomPlayerId(); //TODO enlever le random;
        this.gameTurn();
        /*
            1) Choose a first player (save in local storage), next start will choose in localstorage and choose next in list
        */
    }


    gameTurn() {
        var player = this.getPlayerFromId(this.currentPlayerId);
        console.log('Turn for : ', player.name);
        /*
            1) Who's the first player ?
            2) Take card !
            3) Do an action with this card (takeCard in usedCards, use power, throw it, exchange in player cards game)
            4) End of game turn
        */
    }


    gameTurnAction() {

    }

    useCardPower(card) {
        if (!card.hasPower) {
            return false;
        }

        card.power();
    }

    endGameTurn() {
        var nextCurrentPlayerId = this.currentPlayerId + 1;
        this.currentPlayerId = typeof this.players[nextCurrentPlayerId] === 'undefined' ? 0 : nextCurrentPlayerId;
    }

    getCurrentUser() {
        return this.getPlayerFromId(this.currentPlayerId);
    }
}
