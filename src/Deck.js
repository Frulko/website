import { CARDS_VALUE, SUITS_TYPES, SPECIAL_TYPES, SPECIALS_NAME, SPECIAL_CARDS } from './Const';
import * as CardsClasses from './Cards';


export default class Deck {
    constructor() {
        this.cards = this.generate();
    }

    generate() {

        for(var i=2, l=11; i<l; i++){
            CARDS_VALUE[i] = i;
        }

        let t_cards = [];

        for(var type in SUITS_TYPES) {
            let suit = SUITS_TYPES[type];
            for(var value in CARDS_VALUE) {

                if (value === CARDS_VALUE.JACK) {

                }

                let special_card = this.checkSpecialGift(CARDS_VALUE[value], suit);
                let createClass = CardsClasses.PlayingCard;
                if (special_card) {
                    if (special_card.id === SPECIAL_TYPES.OWN_SPY) {
                        createClass = CardsClasses.SelfSpyCard;
                    } else if (special_card.id === SPECIAL_TYPES.OPPONENT) {
                        createClass = CardsClasses.OpponentSpyCard;
                    } else if (special_card.id === SPECIAL_TYPES.BLIND_EXCHANGE) {
                        createClass = CardsClasses.BlindThiefCard;
                    } else if (special_card.id === SPECIAL_TYPES.CLEAR_EXCHANGE) {
                        createClass = CardsClasses.ClearThiefCard;
                    } else {
                        createClass = CardsClasses.PlayingCard;
                    }
                }
                //console.log(SPECIAL_TYPES.OWN_SPY, special_card, createClass)

                let card = new createClass({
                    skin: suit,
                    value: CARDS_VALUE[value]
                });

                t_cards.push(card);
            }
        }

        return this.shuffle(t_cards);
    }

    checkSpecialGift(value, suit) {
        let search = {
            value,
            suit
        };

        let find_value = {};
        let find_suit = {};

        for (var [key, data] of SPECIAL_CARDS) {

            if (typeof data.values !== 'undefined' && (find_value.value === -1 || !find_value.value)) {
                let found_value = data.values.indexOf(search.value);

                if (found_value !== -1) {
                    find_value = {
                        type: SPECIALS_NAME.get(key),
                        id: key,
                        value: search.value
                    };
                } else {
                    find_value.value = found_value;
                }
            }
            if (typeof data.suits !== 'undefined' && (find_suit.value === -1 || !find_suit.value)) {
                let found_suit = data.suits.indexOf(search.suit);
                find_suit.value = found_suit;
                if (found_suit !== -1) {
                    find_suit = {
                        type: SPECIALS_NAME.get(key),
                        id: key,
                        value: search.value
                    };
                }
            }
        }

        if (typeof find_value.type === 'undefined') {
            return false;
        }

        if ((find_value.value && find_suit.value === -1) || (find_value.value && find_suit.value !== -1)) {
            return {type: find_value.type, value:find_value.value, id: find_value.id};
        }

        return false;
    }

    shuffle(array) {
        var m = array.length, t, i;

        // While there remain elements to shuffle…
        while (m) {

            // Pick a remaining element…
            i = Math.floor(Math.random() * m--);

            // And swap it with the current element.
            t = array[m];
            array[m] = array[i];
            array[i] = t;
        }

        return array;
    }
}
