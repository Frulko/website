var mongoose = require('mongoose');
var moment = require('moment');
import _ from 'lodash';
mongoose.connect('mongodb://localhost/test');


var tweets = [
	{
		id: 1,
		title: 'L\'impression 3D au service de la créativité',
		image: 'tumblr_ngzgpjIJLO1rn12fjo1_1280.png',
		category: {
			name: 'Technologie',
			slug: 'tech'
		},
		date: moment().format('DD,MM,YY'),
	},
	{
		id: 12,
		title: 'Spotify compte maintenant plus de 60 millions d’utilisateurs',
		image: 'Autodesk-01.jpg',
		category: {
			name: 'Voyage',
			slug: 'voyage'
		},
		content:`<p>Le week end du 16 Novembre se tenait à Paris le premier 3DPrintShow de France car jusqu'ici ce salon était présent à Londres et à New York. Ce donc fut l'occasion de rencontrer des personnalités et acteurs de l'impression 3D ainsi que conseils et astuces, car je possède moi-même une imprimante 3D.<p>
				<p>Il faut savoir qu'on nous bassine depuis environ un an sur le fait que l'impression 3D est la nouvelle révolution industrielle, que ce nouveau moyen qui permet de créer des objets va permettre de remplacer des objets ou pièces cassés, de créer de nouvelles choses jusque la possible qu'en industrie et que le grand public pourra faire chez lui l'objet qu'il veut.</p>
				<p>Avec l'explosion des réseaux sociaux, de Facebook à Twitter et même en passant par Google+ (et oui il y a quelques irréductibles qui utilisent G+), proposer son idée, la partager à tout le monde permet d'avoir un retour sur un travail et même des conseils pour une évolution de l'idée, rencontrer des spécialiste dans un domaine qui pourront apporter leur expérience.</p>
				<div class="attachment"><img src="https://c2.staticflickr.com/8/7219/27573116001_2cc0f3bbe5_h.jpg" alt="" /><div class="caption-text">La légende de la photo regardez moi ca comme c’est beau</div></div>
				`,
		date: moment().format('DD,MM,YY'),
	},
	{
		id: 3,
		title: 'Terrorisme – C’est encore la faute d’Internet ?',
		image: 'wallhaven-88594.png',
		category: {
			name: 'Web',
			slug: 'web'
		},
		date: moment().format('DD,MM,YY'),
	},
	{
		id: 4,
		title: 'Fuze ou le meilleur projet de ma carrière',
		image: 'fuze.png',
		category: {
			name: 'Projet',
			slug: 'projet'
		},
		date: moment().format('DD,MM,YY'),
	},
	{
		id: 5,
		title: 'MyFox – L’alarme connectée qui approche la perfection',
		image: 'volet.png',
		category: {
			name: 'Voyage',
			slug: 'voyage'
		},
		date: moment().format('DD,MM,YY'),
	}];

var TwitterTweets = mongoose.model('Twitter', {
	tweet: {}
});

function getFirst(limit, callback) {
	TwitterTweets.aggregate([
		{$sort: {'tweet.id_str': -1}},
		{$limit: limit},
	]).exec(function(err, posts) {
		callback(posts);
	});
}

export function fetchTweets(limit = 10, sort = -1, callback) {
	callback(tweets);
	return false;
	TwitterTweets.aggregate([
		{$sort: {'tweet.id_str': sort}},
		{$limit: limit},
	]).exec(function(err, tweets) {

		callback(tweets.map((entry)=>({
			text: entry.tweet.text, 
			id: entry.tweet.id_str,
			date: moment(entry.tweet.created_at, 'dd MMM DD HH:mm:ss ZZ YYYY', 'en').format('MM-DD-YYYY')
		})));
	});
}


export function fetchTweetById(id, callback) {
	
	let res = _.find(tweets, _.matchesProperty('id', id));
	callback(res);
}
