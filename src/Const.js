export const CARDS_VALUE = {
    ACE: 1,
    JACK: 11,
    QUEEN: 12,
    KING: 13
};

export const SUITS_TYPES = {
    CLUBS: 1,
    DIAMONDS: 2,
    HEARTS: 3,
    SPADES: 4
};

export const SPECIAL_TYPES = {
    OWN_SPY: 1,
    OPPONENT: 2,
    BLIND_EXCHANGE: 3,
    CLEAR_EXCHANGE: 4
};

export const SPECIAL_CARDS = new Map([
    [SPECIAL_TYPES.OPPONENT, {values: [9, 10]}],
    [SPECIAL_TYPES.OWN_SPY, {values: [7, 8]}],
    [SPECIAL_TYPES.BLIND_EXCHANGE, {values: [CARDS_VALUE.JACK, CARDS_VALUE.QUEEN]}],
    [SPECIAL_TYPES.CLEAR_EXCHANGE, {values: [CARDS_VALUE.KING], suits: [SUITS_TYPES.CLUBS, SUITS_TYPES.SPADES]}]
]);

export const SUITS_NAME = new Map([
    [SUITS_TYPES.CLUBS, 'clubs'],
    [SUITS_TYPES.DIAMONDS, 'diamonds'],
    [SUITS_TYPES.HEARTS, 'hearts'],
    [SUITS_TYPES.SPADES, 'spades']
]);

export const VALUES_NAME = new Map([
    [CARDS_VALUE.ACE, 'ace'],
    [CARDS_VALUE.JACK, 'jack'],
    [CARDS_VALUE.QUEEN, 'queen'],
    [CARDS_VALUE.KING, 'king']
]);

export const SPECIALS_NAME = new Map([
    [SPECIAL_TYPES.OWN_SPY, 'OWN_SPY'],
    [SPECIAL_TYPES.OPPONENT, 'OPPONENT'],
    [SPECIAL_TYPES.BLIND_EXCHANGE, 'BLIND_EXCHANGE'],
    [SPECIAL_TYPES.CLEAR_EXCHANGE, 'CLEAR_EXCHANGE']
]);
