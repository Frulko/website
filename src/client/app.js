import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, combineReducers, compose, applyMiddleware } from 'redux'
import { Router, browserHistory, hashHistory } from 'react-router'
import App from '../components/App';
import routes from '../routes';
import NormalizeCSS from 'normalize.css/normalize.css';
import tweetsApp from '../reducers';
import {addTweet} from '../actions';
import configureStore from '../configureStore';

import { syncHistoryWithStore, routerReducer, routerMiddleware } from 'react-router-redux'

import style from '../assets/app.scss';

document.addEventListener('DOMContentLoaded', (event) => {

	let historyMethod = hashHistory
	if (__PROD__) {
    	historyMethod = browserHistory;
    }

	const initialState = window.__INITIAL_STATE__;
    const store = configureStore(initialState, historyMethod);
    
	console.log('Ready !');
	render(
		<Provider store={store}>
			 <Router history={historyMethod} routes={routes} />
		</Provider>,
		document.getElementById('app')
	);
});

if (module.hot) {
	module.hot.accept();
}