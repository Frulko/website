import { connect } from 'react-redux'
import { fetchPostIfNeeded } from '../actions/post'
import Post from '../components/Post'
import { provideHooks } from 'redial';

const mapStateToProps = (state) => {
  console.log(state.postsReducer);
  return {
    post: state.postsReducer.post
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onTweetClick: (id) => {
      //dispatch(toggleTodo(id))
    },
    fetchPostIfNeeded: (post_id) => {
      dispatch(fetchPostIfNeeded(post_id))
    }
  }
}

Post.need = [(post_id) => { return fetchPostIfNeeded(post_id); }];
const PostContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Post);

export default PostContainer