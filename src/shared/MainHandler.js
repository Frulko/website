/*import React, {Component} from 'react';
import { Router, Route, Link, browserHistory, IndexRoute } from 'react-router'

import Header from './components/Header/Header'
import PostsListingContainer from '../containers/PostsListingContainer'
import Home from '../containers/HomeContainer'
import Article from '../components/Article';
import App from '../components/App';

const styleSheet = require('./main.css');



export default (
  <Route path='/' component={App}>
  	<IndexRoute component={PostsListingContainer}/>
  	<Route path="/article/:articleID" component={Article}/>
  	<Route path="/articles" component={Article}/>
  </Route>
);*/


import React, {Component} from 'react';
import { Router, Route, Link, browserHistory, IndexRoute } from 'react-router'

import PostsListingContainer from '../containers/PostsListingContainer'
import PostContainer from '../containers/PostContainer';

import App from '../components/App';

export default (
  <Route path='/' component={App}>
  	<IndexRoute component={PostsListingContainer}/>
  	<Route path="/article/:articleID" component={PostContainer}/>
  	<Route path="/articles" component={PostsListingContainer}/>
  </Route>
);