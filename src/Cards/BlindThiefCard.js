import ThiefCard from './ThiefCard';

export default class BlindThiefCard extends ThiefCard{
    constructor(options) {
        super(options);
    }

    power() {
        super.power();
        console.log('BLIND THIEF');
    }
}