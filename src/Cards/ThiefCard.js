import PlayingCard from './PlayingCard';

export default class ThiefCard extends PlayingCard{
    constructor(options) {
        super(options);
    }

    power() {
        super.power();
        console.log('THIEF');
    }
}
