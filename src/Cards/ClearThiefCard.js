import ThiefCard from './ThiefCard';

export default class ClearThiefCard extends ThiefCard{
    constructor(options) {
        super(options);
    }

    power() {
        super.power();
        console.log('CLEAR THIEF');
    }
}