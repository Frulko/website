import PlayingCard from './PlayingCard';

export default class SpyCard extends PlayingCard{
    constructor(options) {
        super(options);
    }

    power() {
        super.power();
        console.log('SPY');
    }
}
