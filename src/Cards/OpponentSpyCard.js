import SpyCard from './SpyCard';

export default class OpponentSpyCard extends SpyCard{
    constructor(options) {
        super(options);
    }

    power() {
        super.power();
        console.log('OPPONENT');
    }
}