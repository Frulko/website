import { VALUES_NAME, SUITS_NAME } from '../Const';

export default class PlayingCard {
    constructor(options) {
        let {value, skin} = options;

        this.inHand = false;
        this.value = value;
        this.skin = skin;

        let valueName = VALUES_NAME.get(this.value) ? VALUES_NAME.get(this.value) : this.value;
        this.valueName = valueName;
        this.skinName =  SUITS_NAME.get(this.skin);
        //this.gift = this.checkSpecialGift();
    }

    power() {
        if (this.inHand) {
            console.log('InHand');
            return false;
        }
    }
}
