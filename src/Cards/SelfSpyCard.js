import SpyCard from './SpyCard';

export default class SelfSpyCard extends SpyCard{
    constructor(options) {
        super(options);
    }

    power() {
        super.power();
        console.log('SELF SPY');
    }
}