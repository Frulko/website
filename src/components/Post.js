import React from 'react'
import { Link } from 'react-router'

export default React.createClass({
	componentWillMount() {
		let {fetchPostIfNeeded, params} = this.props;
		fetchPostIfNeeded(params.articleID);
	},
	render() {
		let {title, image, content} = this.props.post;
		if (typeof image === 'undefined') {
			return false;
		}

		content = content ? content : '';
		console.log('image', image);

		let bgStyle = {
			background: `url(../assets/img/${image}) no-repeat center center fixed`,
			backgroundSize: 'cover',
			width: '100%',
			height: '412px',
			opacity: '0.1'
		};

		let bgContainerStyle = {
			backgroundColor: '#f7f0ef',
			width: '100%',
			height: '412px',
			position: 'absolute',
			top: 0,
			left: 0,
			zIndex: 0
		};

		let styles = {

			title: {
				fontFamily: 'NocturnoDisplay-Med',
				fontSize: 40,
				width: '80%',
				color: '#333333'
			},
			featuredImage: {
				marginTop: 58,
				width: '100%',
				minHeight: 'auto'
			},
			content: {
				marginBottom: 146
			},

			commentContainer: {
				width: '100%',
				minHeight: 250,
				backgroundColor: '#fff'
			},

			commentTitle: {
				fontFamily: 'NocturnoDisplay-Med',
				fontSize: 30,
				color: '#333333'
			}

		};


		return (
			<div className={'post-single'} style={{marginTop: '-53px', position: 'relative'}}>
				<div className={'post-background'} style={bgContainerStyle}>
					<div style={bgStyle}></div>
				</div>

				<article style={{position: 'relative', zIndex: 1, width: 634, margin: '0 auto', paddingTop: 120}}>
					<h1 style={styles.title}>{title}</h1>
					<div>
						<img style={styles.featuredImage} src={'/' + require('../assets/img/' + image)} />
					</div>

					<div className={'post-single-content'} style={styles.content} dangerouslySetInnerHTML={{__html: content}}/>
				</article>

				<footer style={styles.commentContainer}>
					<div style={{position: 'relative', zIndex: 1, width: 634, margin: '0 auto', paddingTop: 40}}>
						<h1 style={styles.commentTitle}>Ajouter un commentaire</h1>
					</div>
					

				</footer>

				
			</div>
		);
	}
})