import React, { Component } from 'react';
import PostsListingItem from './PostsListingItem.js';
import Masonry from 'react-masonry-component';


export default class PostsListing extends Component {

	componentWillMount() {
		let {posts, fetchPostsIfNeeded} = this.props;
		fetchPostsIfNeeded();
	}

	render() {
		let {posts, onTweetClick} = this.props;
		console.log(posts);
		let masonryOptions = {
		    transitionDuration: 0,
		    columnWidth: 1
		};

		let style = {
			width: 827,
			margin: '0 auto'
		};

		return (

			<div style={style}>
				{posts.map(tweet =>
			      <PostsListingItem
			        key={tweet.id}
			       	item={tweet}
			        onClick={() => onTweetClick(tweet.id)}
			      />
			    )}
			</div>
		);
	}
}

