import React from 'react'
import NavLink from './NavLink';

class Header extends React.Component{
	render() {
		return (
			<div className="top-header">
				<div className="background-menu">
					<img src='/assets/img/background.png' />
				</div>
				<div className="background-color"></div>
				<ul className="menu-items">
					<li><NavLink to="/articles">Articles</NavLink></li>
					<li><NavLink to="/projects">Projets</NavLink></li>
					<li><NavLink to="/" className={'logo'}> <i className="logo-icons"></i> </NavLink></li>
					<li><NavLink to="/photos">Photos</NavLink></li>
					<li><NavLink to="/about">À propos</NavLink></li>
				</ul>
			</div>
		);
	}
};

export default Header;