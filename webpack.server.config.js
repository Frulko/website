var PROD = process.argv.indexOf("--production") > -1;
var config = require('./package.json');
var nodeExternals = require('webpack-node-externals');
var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var WebpackNotifierPlugin = require('webpack-notifier');
var ExternalsPlugin = require('webpack-externals-plugin');
var extractCSS = new ExtractTextPlugin('../assets/style.css', {
	disable: !PROD,
	allChunks: PROD
});

var definePlugin = new webpack.DefinePlugin({
  __DEV__: !PROD,
  __PROD__: PROD
});

var plugins = {
    production: [
        new webpack.BannerPlugin('require("source-map-support").install();',
                           { raw: true, entryOnly: false }),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.NoErrorsPlugin(),
        extractCSS,
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            },
            sourceMap: false
        }),
        definePlugin
    ],
    developpement: [
        new webpack.BannerPlugin('require("source-map-support").install();',
                           { raw: true, entryOnly: false }),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin(),
        extractCSS,
        new WebpackNotifierPlugin({
            title: config.name,
            excludeWarnings: true
        }),
        definePlugin
    ]
};

module.exports = {
	entry: path.join(__dirname, 'src', 'server', 'app.js'),
	target: 'node',
	output: {
		path: path.join(__dirname, 'dist', 'server'),
		filename: 'server.js'
	},
    externals: [nodeExternals()],
	alias: {
      'app': path.join(__dirname, 'src', 'app'),
      'styles': path.join(__dirname, 'src', 'scss'),
      'assets': path.join(__dirname, 'src', 'assets'),
      'plop': path.join(__dirname, 'src', 'assets'),
      'fonts': path.join(__dirname, 'src', 'assets', 'fonts')
    },
	module: {
		loaders: [
			{
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel',
                query: {
                    presets: ['react', 'es2015', 'stage-0']
                }
            },
            {
                test: /\.json$/,
                loaders: ['json']
            },
            {
                test: /\.(ico|jpe?g|png|gif)$/,
                loaders: [
                  "file?name=[path][name].[ext]&context=./src",
                ],
            },
            {
                test: /\.(woff|woff2|ttf|otf|eot|svg)$/,
                loaders: [
                  "file?name=[path][name].[ext]&context=./src",
                ],
            },
            {
                test: /\.jade$/,
                loader: 'jade'
            },
            {
                test: /\.css$/,
                loader: PROD ? extractCSS.extract('css') : 'style!css'
            },
            {
                test: /\.scss$/i,
                loader: PROD ? extractCSS.extract('css!sass') : 'style!css!sass'
            },
		]
	},
	plugins: PROD ? plugins.production : plugins.developpement
	
};